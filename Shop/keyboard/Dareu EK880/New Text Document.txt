Bàn Phím cơ Dareu EK880
Thiết kế gọn gàng, tối giản không gian sử dụng
Build Quality chắc chắn, cực kì cứng cáp
Font chữ được cải tiến mang phong cách Cao Cấp
Loại switch cơ học D-Switch được cải tiến có chất lượng gõ tốt hơn.
Đèn LED RGB sáng hơn và có hiệu ứng chuyển động mượt mà hơn
Sử dụng loại Switch phím cơ của Dare-U mang tên D-Switch
Có cả 3 loại switch phù hợp với các nhu cầu khác nhau