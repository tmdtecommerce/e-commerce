const slides = document.querySelector(".slides").children;
const slideTime = 5000;
let index = 0;

let h1 = document.getElementById("slide-show").offsetHeight;
let h2 = document.getElementById("product").offsetHeight;

function getSideBarAndMenuHeight(){
    document.querySelector(".side-bar").style.height = (h1 + h2 + 10)+"px";
    document.querySelector("#menu").style.height = (h1 + h2 + 50)+"px";
}



function slideShow(){
    
    for(let i=0;i<slides.length;i++)
    {
        slides[i].classList.remove("active");
    }

    slides[index].classList.add("active");

    if(index == slides.length-1){
        index=0;
    }
    else{
        index++;
    }

    setTimeout(()=>{
        slideShow();
    },slideTime);
}

window.onload=()=>{
    slideShow();
    getSideBarAndMenuHeight();
}